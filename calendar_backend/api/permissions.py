from rest_framework.permissions import BasePermission
from rest_framework.authtoken.models import Token
from rest_framework.authentication import SessionAuthentication


class HaveTokenPermission(BasePermission):

    def has_permission(self, request, view):
        request_token = request.META.get('HTTP_WWW_AUTHENTICATE')
        try:
            Token.objects.get(user=request.user, key=request_token)
            return True
        except Token.DoesNotExist:
            pass
        return False


class CsrfExempt(SessionAuthentication):
    def enforce_csrf(self, request):
        return None
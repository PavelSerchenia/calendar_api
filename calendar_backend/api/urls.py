from django.urls import path, include

urlpatterns = [
    path('auth/', include('auth_module.urls')),
    path('events/', include('events.urls')),
    path('holidays/', include('country_holidays.urls'))

]


from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class CountryHoliday(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='country_holiday')
    name = models.CharField(max_length=256)
    date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name

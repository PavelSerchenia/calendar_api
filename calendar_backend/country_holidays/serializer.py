from rest_framework import serializers
from .models import CountryHoliday


class MonthHolidaysSerializer(serializers.ModelSerializer):
    class Meta:
        model = CountryHoliday
        fields = ('name', 'date')




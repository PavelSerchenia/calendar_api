from django.apps import AppConfig


class CountryHolidaysConfig(AppConfig):
    name = 'country_holidays'

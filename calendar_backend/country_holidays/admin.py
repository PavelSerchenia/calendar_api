from django.contrib import admin
from .models import CountryHoliday, Country

admin.site.register(CountryHoliday)
admin.site.register(Country)

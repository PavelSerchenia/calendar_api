from rest_framework.generics import ListAPIView
from .serializer import MonthHolidaysSerializer
from rest_framework.permissions import IsAuthenticated
import datetime

from api.permissions import HaveTokenPermission, CsrfExempt
from .models import CountryHoliday, Country


class MonthHolidaysView(ListAPIView):
    serializer_class = MonthHolidaysSerializer
    permission_classes = [IsAuthenticated, HaveTokenPermission]
    authentication_classes = (CsrfExempt,)

    def get_queryset(self):
        user_country = self.request.user.calendar_user.country_id
        country = Country.objects.get(id=user_country)
        today = datetime.datetime.today()
        return CountryHoliday.objects.filter(country=country, date__year=today.year, date__month=self.kwargs['month'])
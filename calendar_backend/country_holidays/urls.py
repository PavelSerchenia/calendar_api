from django.urls import path
from .views import MonthHolidaysView
urlpatterns = [
    path('<int:month>', MonthHolidaysView.as_view(), name="holidays"),
]

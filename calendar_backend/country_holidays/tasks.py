from __future__ import absolute_import, unicode_literals
from ics import Calendar
import requests
from billiard.pool import Pool
from calendar_backend.celery import app
from .models import CountryHoliday, Country
import re
from django.core.mail import send_mail
from django.conf import settings


@app.task
def get_country_holidays(country):
    req = Calendar(requests.get(f'https://www.officeholidays.com/ics/ics_country.php?tbl_country={country}').text)
    country_events = req.events
    country_model = Country.objects.get(name=country)
    for data in country_events:
        re_holiday = re.search(" [a-zA-Z0-9 ',]*", data.name).group()
        holiday = (re_holiday[0:])
        country_holiday, created = CountryHoliday.objects.get_or_create(country=country_model, name=holiday)
        country_holiday.date = f"{data.begin.year}-{data.begin.month}-{data.begin.day}"
        country_holiday.save()

@app.task
def update_holidays():
    countries = Country.objects.all()
    pool = Pool(3)
    pool.map(updater, countries)


def updater(country):
    req = Calendar(requests.get(f'https://www.officeholidays.com/ics/ics_country.php?tbl_country={country.name}').text)
    country_events = req.events
    for data in country_events:
        re_holiday = re.search(" [a-zA-Z0-9 ',]*", data.name).group()
        holiday = (re_holiday[0:])
        country_holiday, created = CountryHoliday.objects.get_or_create(country=country, name=holiday)
        country_holiday.date = f"{data.begin.year}-{data.begin.month}-{data.begin.day}"
        country_holiday.save()


@app.task(name="notify_message")
def notify_message(target_mail, text):

    send_mail(
        subject='Notify',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[target_mail],
        fail_silently=False,
        message=text,
    )





from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'calendar_backend.settings')
app = Celery('calendar_backend', broker=settings.CELERY_BROKER_URL, backend=settings.CELERY_RESULT_BACKEND)

app.config_from_object('django.conf:settings', namespace='CELERY')
app.config_from_object('django.conf:settings')
app.conf.beat_schedule = {
    'update_holidays': {
        'task': 'country_holidays.tasks.update_holidays',
        'schedule': 10080,
    }
}
app.conf.timezone = 'UTC'
app.autodiscover_tasks()


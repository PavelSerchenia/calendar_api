from django.contrib import admin
from .models import CalendarUser

admin.site.register(CalendarUser)

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.contrib.auth import login

from .models import User, CalendarUser
from country_holidays.models import Country
from country_holidays.tasks import get_country_holidays


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('name',)


class SignUpSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    country = CountrySerializer(required=False)

    class Meta:
        model = CalendarUser
        fields = ('user', 'country')

    def create(self, validated_data):
        if not User.objects.filter(email=validated_data['user']['email']).exists():
            try:
                country, created = Country.objects.get_or_create(name=validated_data.get('country').get('name').capitalize())
                if created:
                    get_country_holidays.apply_async(
                        kwargs={'country': validated_data.get('country').get('name').capitalize()})
            except AttributeError:
                country = None
            user = User.objects.create(username=validated_data['user']['username'],
                                       email=validated_data['user']['email'])
            user.set_password(validated_data['user']['password'])
            user.save()
            CalendarUser.objects.create(user=user, country=country)
            return user
        else:
            raise ValidationError(detail='Email exists')


class SignInSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('email', 'password')

    def create(self, validated_data):
        user = self.authenticate(email=validated_data['email'], password=validated_data['password'])
        if user is not None:
            login(self.context['request'], user)
            return user
        else:
            raise ValidationError(detail="Not valid data")

    def authenticate(self, email=None, password=None):
        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

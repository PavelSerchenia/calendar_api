# Generated by Django 2.1.5 on 2019-01-30 20:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth_module', '0003_auto_20190130_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendaruser',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_country', to='auth_module.Country'),
        ),
    ]

# Generated by Django 2.1.5 on 2019-01-30 22:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('country_holidays', '0003_auto_20190130_2229'),
        ('auth_module', '0004_auto_20190130_2053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendaruser',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_country', to='country_holidays.Country'),
        ),
        migrations.DeleteModel(
            name='Country',
        ),
    ]

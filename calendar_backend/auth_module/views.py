from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from .serializer import SignUpSerializer, SignInSerializer


class SignUpView(CreateAPIView):
    serializer_class = SignUpSerializer

    def post(self, request, *args, **kwargs):
        serializer = SignUpSerializer(data=request.data)
        data = None
        response_status = None
        if serializer.is_valid():
            response_status = status.HTTP_201_CREATED
            serializer.save()
        else:
            data = serializer.errors
            response_status = status.HTTP_400_BAD_REQUEST
        return Response(data=data, status=response_status)


class SignInView(CreateAPIView):
    serializer_class = SignInSerializer

    def post(self, request, *args, **kwargs):
        serializer = SignInSerializer(data=request.data, context={'request': request})
        data = None
        response_status = None
        if serializer.is_valid():
            user = serializer.save()
            token, created = Token.objects.get_or_create(user=user)
            response_status = status.HTTP_200_OK
            data = {"token": token.key,
                    "user_id": user.id
                     }
        else:
            data = serializer.errors
            response_status = status.HTTP_400_BAD_REQUEST
        return Response(data=data, status=response_status)

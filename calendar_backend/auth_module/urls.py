from django.urls import path
from .views import SignUpView, SignInView
urlpatterns = [
    path('sign_up/', SignUpView.as_view(), name="sign-up"),
    path('sign_in/', SignInView.as_view(), name="sign-in"),
]


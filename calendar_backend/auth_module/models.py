from django.db import models
from django.contrib.auth.models import User


class CalendarUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='calendar_user')
    country = models.ForeignKey("country_holidays.Country", on_delete=models.CASCADE, related_name='user_country', blank=True, null=True)

    def __str__(self):
        return str(self.user)
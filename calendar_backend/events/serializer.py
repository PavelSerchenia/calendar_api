from rest_framework import serializers
from datetime import datetime, timedelta
from rest_framework.exceptions import ValidationError

from .models import UserEvents
from country_holidays.tasks import notify_message


class CreateEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserEvents
        fields = ('name', 'date_start', 'date_end', 'notify_time')

    def create(self, validated_data):
        request_date_end = validated_data.get('date_end')
        # if notify value is none set as end of the day
        date_end = datetime.today().replace(hour=23, minute=59, second=59) if request_date_end is None else\
            request_date_end
        date_state = self.check_date(validated_data['date_start'], date_end)
        if date_state:
            new_event = UserEvents.objects.create(user=self.context['user'].calendar_user,
                                                  name=validated_data['name'],
                                                  date_start=validated_data['date_start'],
                                                  date_end=date_end,
                                                  notify_time=validated_data.get('notify_time'))
            if validated_data.get('notify_time') is not None:
                # check if notify is available
                value = datetime.today() + timedelta(minutes=validated_data.get('notify_time'))
                if value <= date_end.replace(tzinfo=None):
                    date_to_start = date_end - timedelta(minutes=validated_data.get('notify_time'))
                    notify_message.apply_async(eta=date_to_start, kwargs={'target_mail': self.context['user'].email,
                                                                          'text': validated_data['name']})
            return new_event
        else:
            raise ValidationError(detail={'error': 'Date has expired'})

    # check date for valid
    @staticmethod
    def check_date(date_start, date_end):
        today = datetime.today()
        date_end = date_end.replace(tzinfo=None)
        date_start = date_start.replace(tzinfo=None)
        if not date_end < today and not date_start < today and date_end != date_start:
            return True

    # custom input data to pass human readable value instead of model value
    def is_valid(self, raise_exception=False):
        notify_time = self.initial_data.get('notify_time')
        for time in UserEvents.minutes_to_notify:
            if time[1] == notify_time:
                self.initial_data['notify_time'] = time[0]
        return super(CreateEventSerializer, self).is_valid(raise_exception=False)


class MonthEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserEvents
        fields = ('name', 'date_start', 'date_end', 'notify_time')

    def to_representation(self, instance):
        data = super(MonthEventSerializer, self).to_representation(instance)
        data.update({"day": instance.date_end.day})
        return data




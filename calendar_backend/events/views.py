from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
import datetime

from .serializer import CreateEventSerializer, MonthEventSerializer
from .models import UserEvents
from api.permissions import HaveTokenPermission, CsrfExempt


class CreateEventView(CreateAPIView):
    serializer_class = CreateEventSerializer
    permission_classes = [IsAuthenticated, HaveTokenPermission]
    authentication_classes = (CsrfExempt,)

    def post(self, request, *args, **kwargs):
        serializer = CreateEventSerializer(data=request.data, context={'user': request.user})
        data = None
        response_status = None
        if serializer.is_valid():
            serializer.save()
            response_status = status.HTTP_201_CREATED
        else:
            data = serializer.errors
            response_status = status.HTTP_400_BAD_REQUEST
        return Response(data=data, status=response_status)


class DayEventsView(ListAPIView):
    serializer_class = CreateEventSerializer
    permission_classes = [IsAuthenticated, HaveTokenPermission]
    authentication_classes = (CsrfExempt,)

    def get_queryset(self):
        date_start = datetime.datetime.today().replace(hour=00, minute=00, second=00)
        date_end = datetime.datetime.today().replace(hour=23, minute=59, second=59)
        return UserEvents.objects.filter(user=self.request.user.calendar_user, date_end__range=(date_start, date_end))


class MonthEventsView(ListAPIView):
    serializer_class = MonthEventSerializer
    permission_classes = [IsAuthenticated, HaveTokenPermission]
    authentication_classes = (CsrfExempt,)

    def get_queryset(self):
        today_date = datetime.datetime.today()
        return UserEvents.objects.filter(user=self.request.user.calendar_user, date_end__month=today_date.month,
                                         date_end__year=today_date.year)
from django.urls import path
from .views import CreateEventView, DayEventsView, MonthEventsView

urlpatterns = [
    path('create_event/', CreateEventView.as_view(), name="event-creation"),
    path('day_events/', DayEventsView.as_view(), name="day-events "),
    path('month_events/', MonthEventsView.as_view(), name="month-events "),
]
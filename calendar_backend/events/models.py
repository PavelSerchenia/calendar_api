from django.db import models


class UserEvents(models.Model):
    hour = 60
    two_hours = 120
    four_hours = 240
    day = 1440
    week = 10080
    minutes_to_notify = (
        (hour, "За час"),
        (two_hours, "За 2 часа"),
        (four_hours, "За 4 часа"),
        (day, "За день"),
        (week, "За неделю")

    )
    user = models.ForeignKey('auth_module.CalendarUser', on_delete=models.CASCADE, related_name="events_user")
    name = models.CharField(max_length=50)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField(null=True, blank=True)
    notify_time = models.IntegerField(choices=minutes_to_notify, null=True, blank=True)

    def __str__(self):
        return self.name
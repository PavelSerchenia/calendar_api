# calendar_api

Registration
Request with country:
{
	"user":{
		"username":"depewfffdavel",
		"email":"ebffewfduf@gmail.com",
		"password":"test"
	},
	"country":{
		"name":"Belarus"
	}
}
Without country:
{
	"user":{
		"username":"dedpewfffdavel",
		"email":"ebfffewfd@gmail.com",
		"password":"test"
	}
}

Login
{
	"email":"test@gmail.com",
	"password":"test"
}
Response:
{
    "token": "85c5bb6043f6459fc3e1a41af3de51537771255e",
    "user_id": 23
}
Then in any user request to backend need to pass token in 
'WWW-Authenticate' header

Event creation
{
	"name":"Визит к стоматологу",
	"date_start":"2019-02-01 10:59",
	"notify_time":"За 4 часа",
	"date_end":"2019-02-01 11:59"
}

Without notification
{
	"name":"Визит к стоматологу",
	"date_start":"2019-02-01 10:59",
	"date_end":"2019-02-01 11:59"
}

To run periodic holidays updates need to run celery beat


Urls:
Register: http://127.0.0.1:8000/api/auth/sign_up/
Login: http://127.0.0.1:8000/api/auth/sign_in/
Event creation: http://127.0.0.1:8000/api/events/create_event/
Day events: http://127.0.0.1:8000/api/events/day_events/
Month events aggregation: http://127.0.0.1:8000/api/events/month_events/
Holidays by month http://127.0.0.1:8000/api/holidays/{int:month}






